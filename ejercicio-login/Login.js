const express = require('express');
const cors = require('cors')
const port = 3000;
const passport = require('passport');
const public_routes = require('./routes/public');
const auth_routes = require('./routes/auth');
const payment_routes = require('./routes/payment');
require('./services');

const app = express();
// Add headers before the routes are defined
app.use(cors());

// Initializes passport and passport sessions
app.use(passport.initialize());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());
app.use(public_routes);
app.use(auth_routes);
app.use(payment_routes); 

//Nuevo usuario
app.post('/newuser', (req, res) => {
    
    const name = req.body.email;
    const mail = req.body.nombre_y_apellido;
    const pass = req.body.pass;
    const cPass = req.body.confirm_pass;


    let data = {
        'success': true,
        'message': `User ${req.body.nombre_y_apellido} registered correctly`,
        'data': req.body
      }
      res.json(data);
})

//Login
app.post('/login', (req, res) => {
    console.log(req.body)
    res.sendStatus(200)

})

//Servidor config
const PORT = 3000;
app.set('port', PORT);

//Levantando servidor
app.listen(PORT, () => {
    console.log(`Servidor ${app.get('port')} funcionando`)

})